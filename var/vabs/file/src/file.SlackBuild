#!/bin/sh

# Copyright 2005-2008, 2009, 2010  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
NAME=file
VERSION=${VERSION:-5.11}
NUMJOBS=${NUMJOBS:--j4}
LINK=${LINK:-"ftp://ftp.astron.com/pub/$NAME/$NAME-$VERSION.tar.gz"}

#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-""} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

if [ "$NORUN" != 1 ]; then

#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------

#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-file

rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP
rm -rf file-$VERSION
tar xvf $CWD/file-$VERSION.tar.?z* || exit 1
cd file-$VERSION || exit 1
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

# Make the default data directory /etc/file like it has always been:
#zcat $CWD/file.etc.file.diff.gz | patch -p1 --verbose || exit 1
# This is needed after the patch above:
#autoreconf || exit 1

# Don't spew warnings about using the flat text files
zcat $CWD/file.quiet.diff.gz | patch -p1 -E --verbose || exit 1
# I don't know what this one does
zcat $CWD/file.short.diff.gz | patch -p1 -E --verbose || exit 1

# Add zisofs and crda regulatory bin detection
zcat $CWD/file.zisofs.magic.gz >> magic/Magdir/compress
zcat $CWD/file.crdaregbin.magic.gz >> magic/Magdir/crdaregbin

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --datadir=/etc \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --docdir=/usr/doc/file-$VERSION \
  --enable-fsect-man5 \
  --disable-static \
  --build=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1

# The generated $TMP/file-$VERSION/libtool is buggy.  I don't know if this is supposed
# to work, but it certainly does, so... :-)
cat $(which libtool) > libtool

make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG || exit 1

# Seems --disable-static has been ignored lately:
rm -f $PKG/usr/lib${LIBDIRSUFFIX}/libmagic.a

# Is file really this much of a processing bottleneck?  Doubtful.
#
# NOTE:  Benchmarked flat files vs. .mgc June 2009, found no measurable
# difference on 3000+ files.
#
# If you really need these (let's say you're doing virus scanning
# and this *would* speed things up quite a bit) you can create
# the pre-parsed file yourself using file's -C option.
rm -f $PKG/etc/file/magic.mgc
mkdir -p $PKG/etc/file/magic
cp -a magic/Magdir/* $PKG/etc/file/magic
# After building, this works fine.  /etc/file/magic/ takes up 1.2M,
# while the magic.mgc file is 1.7M.  Considering a difference of 500k,
# and the potential speedup, I'm not sure it's worth the effort...
# but it does make it easier for an admin to add a small chunk of
# new magic.

# IMHO, moving this sort of thing does not make sense.
# We'll support both the traditional and new locations.
# I hate to squat on a name as generic as "/etc/misc/",
# but it wasn't my idea.
( cd $PKG/etc ; ln -sf file misc )

# Strip everything for good measure:
find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

( cd $PKG/usr/man || exit 1
  find . -type f -exec gzip -9 {} \;
  for i in $(find . -type l) ; do ln -s $(readlink $i).gz $i.gz ; rm $i ; done
)

mkdir -p $PKG/usr/doc/file-$VERSION
cp -a \
  AUTHORS COPYING INSTALL MAINT NEWS README TODO \
    $PKG/usr/doc/file-$VERSION

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

# Build the package:
cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP
fi

