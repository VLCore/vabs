#!/bin/sh
#-- ffmpeg for Slackware --
# Build script by Phantom X <megaphantomx@bol.com.brg>
# Suggested usage: $ ffmpeg.SlackBuild 2>&1 | tee build.log
#--
# Copyright 2008, 2009, 2010 Phantom X, Goiania, Brazil.
# Copyright 2006 Martijn Dekker, Groningen, Netherlands.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR `AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# http://ffmpeg.mplayerhq.hu/

PACKAGER_ID=${PACKAGER_ID:-$USER}
PACKAGER=${PACKAGER:-$USER@$HOSTNAME}
MAKEDEPENDS=${MAKEDEPENDS:-"alsa-lib libva pulseaudio lame vo-aacenc opencore-amr vo-amrwbenc libaacplus libraw1394 libavc1394 libdc1394 faac faad2 speex x264 gsm orc schroedinger rtmpdump libvpx fribidi libass v4l-utils libnut openjpeg libtheora libogg libvorbis xvidcore SDL"}
TAG=${TAG:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
# Set YES for native build with gcc >= 4.2
SB_NATIVE=${SB_NATIVE:-YES}

# Set to YES to replicate slackbuild and patches
SB_REP=${SB_REP:-YES}

CWD=$(pwd)
TMP=${TMP:-/tmp}
if [ ! -d ${TMP} ]; then
  mkdir -p ${TMP}
fi

SNAPBUILD=${SNAPBUILD:-NO}

NAME=ffmpeg
PKG=${PKG:-${TMP}/package-${NAME}}

if [ "${SNAPBUILD}" = "YES" ] ;then
  VERSION=${VERSION:-"$(LC_ALL=C date +%Y%m%d)"}
else
  VERSION=${VERSION:-0.11.2}
fi
#if [ "${SB_NATIVE}" = "YES" ] ;then
#  ARCH=${ARCH:-$(uname -m)}
#else
#  ARCH=${ARCH:-x86_64}
#fi
ARCH=${ARCH:-"$(uname -m)"}
RARCH=${RARCH:-${ARCH}}
if [ "${ARCH}" = "x86_64" ] ;then
  SLKTARGET=${SLKTARGET:-x86_64}
else
  SLKTARGET=${SLKTARGET:-i486}
fi
SLKDTARGET=${SLKDTARGET:-slackware}
SLKDIST=${SLKDIST:-Slackware}
BUILD=${BUILD:-2}
NJOBS=${NJOBS:-$(( $(getconf _NPROCESSORS_ONLN) + 1 ))}
DOCDIR=${PKG}/usr/doc/${NAME}-${VERSION}
SBDIR=${PKG}/usr/src/slackbuilds/${NAME}
PKGDEST=${PKGDEST:-${CWD}/..}
PKGFORMAT=${PKGFORMAT:-txz}
PKGNAME=${NAME}-$(echo ${VERSION} | tr - . )-${ARCH}-${BUILD}${TAG}

# Set to YES to enable nonfree code (unredistributable binaries)
SB_NONFREE=${SB_NONFREE:-YES}

DATE=$(LC_ALL=C date +%d-%b-%Y)

SRCDIR=${NAME}-${VERSION}
if [ "${SNAPBUILD}" = "YES" ] ;then
  SRCARCHIVE=${SRCDIR}.tar.xz
else
  SRCARCHIVE=${SRCDIR}.tar.bz2
fi

DL_PROG=${DL_PROG:-wget}
DL_TO=${DL_TO:-500}
DL_OPTS=${DL_OPTS:-"--timeout=${DL_TO}"}
DL_URL="http://ffmpeg.org/releases/${SRCARCHIVE}"
#http://ffmpeg.org/releases/ffmpeg-0.11.1.tar.bz2
SNAPUPDATE=${NAME}-snapshot.sh
export snap=${VERSION}

# if source is not present, download in source rootdir if possible, or in /tmp
if [ "${SNAPBUILD}" = "YES" ] ;then
  test -r ${CWD}/${SRCARCHIVE} || sh ${CWD}/${SNAPUPDATE} || exit 1
else
  test -r ${CWD}/${SRCARCHIVE} || ${DL_PROG} ${DL_OPTS} ${DL_URL} || exit 1
fi

if [ "${SB_NATIVE}" = "YES" ] ;then
  SLKCFLAGS="-O2 -march=native -mtune=native ${SB_ECFLAGS} -pipe"
else
  case "${ARCH}" in
    i[3-6]86)    SLKCFLAGS="-O2 -march=${ARCH} -mtune=i686"
                 ;;
    x86_64)      SLKCFLAGS="-O2 -fPIC"
                 ;;
    s390|*)      SLKCFLAGS="-O2"
                 ;;
  esac
fi
if [ "${ARCH}" = "x86_64" ] ;then
  LIBDIRSUFFIX="64"
  SLKCFLAGS="${SLKCFLAGS} -fPIC"
else
  LIBDIRSUFFIX=""
  SLKCFLAGS="${SLKCFLAGS} -fomit-frame-pointer"
fi

unset CFLAGS CXXFLAGS CPPFLAGS

if [ -d ${PKG} ]; then
  # Clean up a previous build
  rm -rf ${PKG}
fi
mkdir -p ${PKG}

cd ${TMP}
rm -rf ${SRCDIR}
tar -xvf ${CWD}/${SRCARCHIVE} || exit 1
cd ${SRCDIR} || exit 1

chmod -R u+w,go+r-w,a-s .

if [ -r ${CWD}/apply-patches.sh ]; then
  . ${CWD}/apply-patches.sh || exit 1
fi

# for some reason it tries to #include <X11/Xlib.h>, but doesn't use it
sed -i s:\#define\ HAVE_X11:\#define\ HAVE_LINUX: ffplay.c || exit 1
# To make sure the ffserver test will work
sed -i -e "s:-e debug=off::" tests/ffserver-regression.sh || exit 1

sed -i -e 's/opteron/opteron|athlon64-sse3|k8-sse3|opteron-sse3/g' configure || exit 1

[ "${SB_NONFREE}" = "YES" ] && SB_NONFREEOPTS="--enable-nonfree"
#not working
#  --enable-libopenjpeg \

./configure \
  --prefix=/usr \
  --incdir=/usr/include \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --shlibdir=/usr/lib${LIBDIRSUFFIX} \
  --mandir=/usr/man \
  --arch=${ARCH} \
  --cpu=${RARCH} \
  --extra-cflags="${SLKCFLAGS}" \
  --enable-libvo-aacenc \
  --enable-libvo-amrwbenc \
  --enable-libopencore-amrnb \
  --enable-libopencore-amrwb \
  --enable-libass \
  --enable-runtime-cpudetect \
  --enable-vaapi \
  --disable-vdpau --enable-memalign-hack \
  --enable-libdc1394 --enable-bzlib --enable-zlib \
  --enable-libmp3lame \
  --enable-libfaac \
  --enable-libspeex \
  --enable-x11grab \
  --enable-libpulse \
  --enable-libv4l2 \
  --enable-libgsm \
  --enable-libnut \
  --enable-librtmp \
  --enable-libschroedinger \
  --enable-libxvid \
  --enable-libtheora \
  --enable-libvorbis \
  --enable-libvpx \
  --enable-libx264 \
  --enable-x11grab \
  --enable-avfilter \
  --enable-postproc \
  --enable-pthreads \
  --enable-static \
  --enable-shared \
  --enable-gpl --enable-version3 ${SB_NONFREEOPTS} \
  --disable-debug \
  --disable-optimizations \
  --disable-stripping \
  --extra-version="${SLKDIST}-${BUILD}${PACKAGER_ID}" || exit 1

make -j${NJOBS} V=1 || make V=1 || exit 1
make install DESTDIR=${PKG} V=1 || exit 1

find ${PKG} | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

mkdir ${PKG}/etc
install -m0644 doc/ffserver.conf \
               ${PKG}/etc/ffserver.conf.new || exit 1

# Add a documentation directory:
rm -f doc/{Makefile,*.{1,pl,texi}}
mkdir -p ${DOCDIR}
cp -a \
  COPYING CREDITS Changelog README doc/*.* ${CWD}/ChangeLog.SB \
  ${DOCDIR}/
find ${DOCDIR}/ -type d -print0 | xargs -0 chmod 0755
find ${DOCDIR}/ -type f -print0 | xargs -0 chmod 0644

# Compress and link manpages, if any:
if [ -d ${PKG}/usr/share/man ]; then
  mv ${PKG}/usr/share/man ${PKG}/usr/man
  rmdir ${PKG}/usr/share
fi
if [ -d ${PKG}/usr/man ]; then
  ( cd ${PKG}/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd ${manpagedir}
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink ${eachpage} ).gz ${eachpage}.gz
          rm -f ${eachpage}
        done
        gzip -9 *.?
        # Prevent errors
        rm -f *.gz.gz
      )
    done
  )
fi

mkdir -p ${PKG}/install
cat ${CWD}/slack-desc > ${PKG}/install/slack-desc
cat $CWD/slack-desc > $PKGDEST/slack-desc
#if [ -e $CWD/slack-required ]; then
#	cat $CWD/slack-required > $PKG/install/slack-required
#	cat $CWD/slack-required > $PKGDEST/slack-required
#else
	ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $PKGDEST $PKG
#fi

#cat ${CWD}/slack-required > ${PKG}/install/slack-required

cat > ${PKG}/install/doinst.sh <<EOF
#!/bin/sh
config() {
  NEW="\$1"
  OLD="\$(dirname \$NEW)/\$(basename \$NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r \$OLD ]; then
    mv \$NEW \$OLD
  elif [ "\$(cat \$OLD | md5sum)" = "\$(cat \$NEW | md5sum)" ]; then # toss the redundant copy
    rm \$NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}
## List of conf files to check.  The conf files in your package should end in .new
EOF

( cd ${PKG}
  find etc/ -name *.new -exec echo config {} ';' | sort >> ${PKG}/install/doinst.sh
  echo >> ${PKG}/install/doinst.sh
)

sed -i "s|_PACKAGER|${PACKAGER}|g; s|_BUILD_DATE|${DATE}|g" \
       ${PKG}/install/slack-desc

if [ "${SB_REP}" = "YES" ] ;then
  # Replicate slackbuild and patches
  mkdir -p ${SBDIR}/patches
  install -m0644 ${CWD}/slack-desc ${CWD}/slack-required ${CWD}/ChangeLog.SB \
                 ${CWD}/${SNAPUPDATE} ${CWD}/apply-patches.sh ${SBDIR}/
  install -m0755 ${CWD}/${NAME}.SlackBuild \
                 ${SBDIR}/${NAME}.SlackBuild
  install -m0644 ${CWD}/patches/*.gz \
                 ${SBDIR}/patches/
fi

# Build package:
set +o xtrace        # no longer print commands upon execution

ROOTCOMMANDS="set -o errexit -o xtrace ; cd ${PKG} ;
  /bin/chown --recursive root:root .  ;"

ROOTCOMMANDS="${ROOTCOMMANDS}
  /sbin/makepkg --linkadd y --chown n ${PKGDEST}/${PKGNAME}.${PKGFORMAT} "

if test ${UID} = 0; then
  eval ${ROOTCOMMANDS}
  set +o xtrace
elif test "$(type -t fakeroot)" = 'file'; then
  echo -e "\e[1mEntering fakeroot environment.\e[0m"
  echo ${ROOTCOMMANDS} | fakeroot
else
  echo -e "\e[1mPlease enter your root password.\e[0m (Consider installing fakeroot.)"
  /bin/su -c "${ROOTCOMMANDS}"
fi

# Clean up the extra stuff:
if [ "$1" = "--cleanup" ]; then
  echo "Cleaning..."
  if [ -d ${TMP}/${SRCDIR} ]; then
    rm -rf ${TMP}/${SRCDIR} && echo "${TMP}/${SRCDIR} cleanup completed"
  fi
  if [ -d ${PKG} ]; then
    rm -rf ${PKG} && echo "${PKG} cleanup completed"
  fi
  rmdir ${TMP} && echo "${TMP} cleanup completed"
fi
exit 0
