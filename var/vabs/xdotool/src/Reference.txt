#!/bin/bash

# Slackware build script for xdotool

# Written by B. Watson (yalhcru@gmail.com)

# Licensed under the WTFPL. See http://sam.zoy.org/wtfpl/ for details.

# 20120411 bkw:
# - updated for xdotool 2.20110530.1.
# - install CHANGELIST in doc dir
# - make & install HTML doc instead of raw POD doc
# - fix permissions in examples/

NAME=xdotool
VERSION=${VERSION:-2.20110530.1}
BUILD=${BUILD:-1}
TAG=${TAG:-_SBo}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i486 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

CWD=$(pwd)
TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$NAME
OUTPUT=${OUTPUT:-/tmp}

if [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $NAME-$VERSION
tar xvf $CWD/$NAME-$VERSION.tar.gz
cd $NAME-$VERSION
chown -R root:root .
chmod -R a-s,u+w,go+r-w .

mkdir -p $PKG/usr/bin $PKG/usr/man/man1

make WARNFLAGS="$SLKCFLAGS" PREFIX=/usr INSTALLLIB=$PKG/usr/lib$LIBDIRSUFFIX
make install PREFIX=$PKG/usr INSTALLLIB=$PKG/usr/lib$LIBDIRSUFFIX
make $NAME.html

strip --strip-unneeded $PKG/usr/bin/$NAME
gzip -9 $PKG/usr/man/man1/$NAME.1

chmod 755 examples/*.sh

mkdir -p $PKG/usr/doc/$NAME-$VERSION
cp -a \
  CHANGELIST README COPYRIGHT examples $NAME.html \
  $PKG/usr/doc/$NAME-$VERSION
cat $CWD/$NAME.SlackBuild > $PKG/usr/doc/$NAME-$VERSION/$NAME.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$NAME-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tgz}
