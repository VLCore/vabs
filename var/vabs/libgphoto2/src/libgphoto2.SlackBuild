#!/bin/sh

# Copyright 2007-2008  Frank Caraballo <fecaraballo{at}gmail{dot}com>
# Copyright 2009, 2010  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


NAME=libgphoto2
VERSION=${VERSION:-2.4.14}
BUILDNUM=${BUILDNUM:-"2"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
LINK=${LINK:-"http://sourceforge.net/projects/gphoto/files/libgphoto/$VERSION/$NAME-$VERSION.tar.gz"}  #Enter URL for package here!
MAKEDEPENDS=${MAKEDEPENDS:-"libusbx gd libexif"} #Add deps needed TO BUILD this package here.

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

NUMJOBS=${NUMJOBS:-" -j3 "}

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi
#GET THE SOURCE
#--------------------------------------------
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
        wget --no-check-certificate -c $SRC
fi
done
#--------------------------------------------
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-$NAME

rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP
rm -rf $NAME-$VERSION
tar xvf $CWD/$NAME-$VERSION.tar.?z* || exit 1
cd $NAME-$VERSION
chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# udev-175 bundles usb_id into the udevd binary
zcat $CWD/fix-usb_id-callout-for-udev-175.diff.gz | patch -p1 || exit 1

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --disable-static \
  --mandir=/usr/man \
  --docdir=/usr/doc/$NAME-$VERSION \
  --with-doc-dir=/usr/doc/$NAME-$VERSION \
  --build=$ARCH-slackware-linux

make $NUMJOBS || make || exit 1

# Make sure udev helper scripts are put in the right place and
# install the other utilities to /usr/bin instead of /usr/lib/libgphoto2/
make $NUMJOBS \
  udevscriptdir=/lib/udev \
  utilsdir=/usr/bin \
  || make \
  udevscriptdir=/lib/udev \
  utilsdir=/usr/bin \
  || exit 1
make \
  udevscriptdir=/lib/udev \
  utilsdir=/usr/bin \
  install DESTDIR=$PKG || exit 1

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

# Generate udev rules and hal device information files
# First, we'll have to set (and export) some important variables
LD_LIBRARY_PATH=$PKG/usr/lib${LIBDIRSUFFIX}
CAMLIBS=$PKG/usr/lib${LIBDIRSUFFIX}/$NAME/$VERSION 
LIBDIR=$PKG/usr/lib${LIBDIRSUFFIX}
export LD_LIBRARY_PATH CAMLIBS LIBDIR
# Generate udev rules
mkdir -p $PKG/lib/udev/rules.d
$PKG/usr/bin/print-camera-list udev-rules version 136 mode 0660 owner root group plugdev \
  > $PKG/lib/udev/rules.d/40-libgphoto2.rules
# Generate fdi files
mkdir -p $PKG/usr/share/hal/fdi/information/20thirdparty
$PKG/usr/bin/print-camera-list hal-fdi \
  > $PKG/usr/share/hal/fdi/information/20thirdparty/10-camera-libgphoto2.fdi
$PKG/usr/bin/print-camera-list hal-fdi-device \
  > $PKG/usr/share/hal/fdi/information/20thirdparty/10-camera-libgphoto2-device.fdi
unset LD_LIBRARY_PATH CAMLIBS LIBDIR   # Unset these just in case

# Compress and link manpages, if any:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

mkdir -p $PKG/usr/doc/$NAME-$VERSION
cp -a \
  AUTHORS COPYING* HACKING INSTALL MAINTAINERS \
  NEWS README* TESTERS \
  $PKG/usr/doc/$NAME-$VERSION

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/*-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

# The apidocs are huge, and probably only of interest to developers who will
# very likely use the source code as a reference:
rm -r $PKG/usr/doc/$NAME-$VERSION/apidocs.html
mkdir -p $PKG/usr/doc/$NAME-$VERSION/apidocs.html
cat << EOF > $PKG/usr/doc/$NAME-$VERSION/apidocs.html/README
The complete API documentation may be found in the
libgphoto2 source code archive.
EOF

# This library is not built, but it is useful for users to know why that is:
cp -a camlibs/jl2005a/README.jl2005a $PKG/usr/doc/$NAME-$VERSION/camlibs
chmod 644 $PKG/usr/doc/$NAME-$VERSION/camlibs/README.jl2005a
chown root:root $PKG/usr/doc/$NAME-$VERSION/camlibs/README.jl2005a

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"
makepkg -l y -c n $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz
echo "Cleaning up temp files..." && rm -rf $TMP
echo "Package Complete"

